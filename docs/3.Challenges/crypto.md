---
hide:
  - toc
author: à compléter
title: Liste des challenges
---

# Cryptographie

!!! abstract "Description générale de la catégorie"

    La cryptographie est l'art de protéger une information en la rendant illisible à l'aide de différents procédés, comme, par exemple, des permutations de lettres ou de groupes de lettres, ou encore en utilisant des calculs mathématiques plus ou moins compliqués.

    Dans cette catégorie, on propose divers challenges ayant pour but de se familiariser avec diverses techniques, des plus simples, comme les chiffrements de César ou Vigenère, aux plus compliqués, comme la manipulation de fonctions de hachage, en passant par diverses méthodes classiques, comme les chiffrements en base 64 ou en base 32.

    
    
!!! warning "Autres challenges"

    D'autres challenges dans différentes catégories sont également disponibles sur le <a href='https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/presentation/' target='_blank'>site principal</a> dédié à la cybersécurité.

    Etes-vous prêt à relever les défis ?


<hr style="height:5px;color:red;background-color:red;">

!!! note "Attaque de mots de passe"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐⭐ 1 flag

        **Objectif :** aborder la problématique de la sécurisation des mots de passe

    ![](../ressources_challenges_crypto/AttaqueMDP/image_attaque_mdp.png){: .center }
    
    Membre d'une équipe de cybersécurité, vous avez été mandaté par une entreprise pour tenter de pénétrer sur son intranet...

    [_Accéder au challenge_](../ressources_challenges_crypto/AttaqueMDP/attaque_mdp){ .md-button target="_blank" rel="noopener" }

!!! note "Cryptage renforcé"

    !!! tip "Présentation"

        **Niveau :** SNT, NSI

        **Difficulté :** ⭐⭐⭐ 1 flag

        **Objectif :** manipuler divers procédés de chiffrement

    ![](../ressources_challenges_crypto/CryptageRenforcé/image_cryptage.png){: .center }
    
    Votre mission, si vous l'acceptez : pénétrez à l'intérieur d'un dossier hautement crypté et décodez le message chiffré qui s'y trouve.

    [_Accéder au challenge_](../ressources_challenges_crypto/CryptageRenforcé/cryptage_renforce){ .md-button target="_blank" rel="noopener" }


!!! note "Danger immédiat"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** interpréter des données chiffrées en utilisant une approche OSINT

    ![](../ressources_challenges_crypto/DangerImmediat/image_chall_danger_immediat.png){: .center }
    
    Membre de la <a href='https://www.dgse.gouv.fr/fr' target='_blank'>DGSE</a>, vous venez d'intercepter un message échangé entre les membres haut placés d'un groupe d'activistes.

    Votre mission : déchiffrer le message le plus rapidement possible pour le transmettre à l’équipe d’intervention avant qu’il ne soit trop tard...

    [_Accéder au challenge_](../ressources_challenges_crypto/DangerImmediat/danger_immediat){ .md-button target="_blank" rel="noopener" }


!!! note "Dossier protégé"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** manipuler divers procédés de chiffrement avec de [bons outils](https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/outils/#chiffrement-et-dechiffrement-de-donnees)

    ![](../ressources_challenges_crypto/DossierProtégé/image_dossier.png){: .center }
    
    Notre agent a caché un message secret dans un dossier protégé par un mot de passe.

    Votre mission : pénétrez à l'intérieur de ce dossier et décodez le message avant qu'il ne soit trop tard...

    [_Accéder au challenge_](../ressources_challenges_crypto/DossierProtégé/dossier_protege){ .md-button target="_blank" rel="noopener" }


!!! note "Grille tournante (challenge issu de la plateforme <a href='https://www.challenges-kids.fr/index.php' target='_blank'>challenges-kids.fr</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐⭐ 1 flag

        **Objectif :** comprendre un procédé de chiffrement utilisant une technique ancienne

    ![](../ressources_challenges_crypto/Challenges_externes/Handbuch_der_Kryptographe.png){: .center }
    
    Dix ans après son célèbre tour du monde en 80 jours, Phileas Fogg se lance dans une nouvelle expédition à la 
    poursuite d'un trésor légendaire : celui qui est caché dans la célèbre caverne d'Ali Baba. Après avoir réussi à 
    modifier le mot de passe magique qui protège l'entrée de la caverne, l'explorateur le chiffre avant de le 
    transmettre à ses compagnons de fortune...

    Arriverez-vous à le déchiffrer ?

    <a href='https://www.challenges-kids.fr/index.php' target='_blank'>Challenges-kids.fr</a> est une plateforme ouverte à la contribution et d'aide à l'enseignement du Hacking pour les petits et les grands, qui offre de nombreux challenges de difficultés graduées pour progresser.

    [_Accéder au challenge_](https://www.challenges-kids.fr/categories/crypto/chall7/){ .md-button target="_blank" rel="noopener" }


!!! note "O Tomates"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐ 4 flags (version light) / ⭐⭐ 4 flags (version épicée)

        **Objectif :** manipuler diverses techniques de chiffrement et de détection de structures valides

    ![](../ressources_challenges_crypto/OTomates/chall_o_tomate_intro.png){: .center }
    
    La suite du challenge <a href='https://cybersecurite.forge.apps.education.fr/cyber-2-osint/3.Challenges/ressources_challenges_osint/Qsales/qsales/' target='_blank'>Q Salés</a> dans lequel Roxane mène une enquête pour retrouver un tableau volé.

    Qui est-il réellement ? Parviendra-t-elle à le mettre hors d'état de nuire ?

    Votre mission : aidez Roxane pour venir à bout du voleur... ou pas...

    [_Accéder au challenge light_](../ressources_challenges_crypto/OTomatesLight/o_tomates_light){ .md-button target="_blank" rel="noopener" } [_Accéder au challenge épicé_](../ressources_challenges_crypto/OTomates/o_tomates){ .md-button target="_blank" rel="noopener" }

    !!! warning "Avertissement"

        Il est recommandé d'avoir déjà traité le challenge <a href='https://cybersecurite.forge.apps.education.fr/cyber-2-web-client/3.Challenges/ressources_challenges_web_client/Motifs/motifs/' target='_blank'>Motifs</a> puisque l'on retrouve ici certaines notions qui y ont été abordées


!!! note "Zip, zip, zip hourra !"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag (version light) / ⭐⭐ 1 flag (version soft) / ⭐⭐⭐ 1 flag (version épicée)

        **Objectif :** aborder la problématique du choix de mots de passe solides

    ![](../ressources_challenges_crypto/Zip/chall_zip.png){: .center }
    
    Membre d'une équipe de cybersécurité, vous êtes chargé d'accéder à un serveur contenant des données sensibles volées afin de les détruire avant qu'elles ne tombent entre de mauvaises mains.

    Votre mission : récupérer le mot de passe de l'administrateur du serveur avant qu'il ne soit trop tard...

    [_Accéder au challenge light_](../ressources_challenges_crypto/ZipLight/zip_light){ .md-button target="_blank" rel="noopener" } [_Accéder au challenge soft_](../ressources_challenges_crypto/Zip/zip){ .md-button target="_blank" rel="noopener" } [_Accéder au challenge épicé_](../ressources_challenges_crypto/ZipSpicy/zip_spicy){ .md-button target="_blank" rel="noopener" }

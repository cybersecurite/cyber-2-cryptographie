---
hide:
  - toc
author: à compléter
title: Attaque de mots de passe
---

# Challenge : attaque de mots de passe

![](../image_attaque_mdp.png){: .center }

Membre d’une équipe de cybersécurité, vous avez été choisi par une entreprise pour repérer les éventuelles failles de sécurité de son réseau intranet.

Après avoir mené différentes investigations, vous avez remarqué les points suivants :

 - il existe trois types de session sur les ordinateurs de l'entrepise pour accéder à l'intranet : _invité_, _utilisateur_ et _administrateur_ ;
 - lorsque l'on choisi la session _invité_ ou _utilisateur_, la connexion se fait automatiquement à l'aide d'un mot de passe inconnu prérempli et la personne doit ensuite saisir un mot de passe personnel ;
 - en revanche, la session _administrateur_ n'a pas de mot de passe prérempli et on accède directement à son interface une fois celui-ci saisi et validé. 

Après plusieurs attaques ciblées sur le réseau de cette entreprise, vous avez réussi à récupérer l’archive confidentielle suivante sur la gestion de l’intranet : <a href='../gestion_intranet_confidentiel.zip' download='gestion_intranet_confidentiel.zip'>gestion_intranet_confidentiel.zip</a>

!!! note "Votre objectif"

    Obtenir le mot de passe de l'administrateur.
    
    Le flag est le SHA-256 de ce mot de passe.


??? abstract "Indice"
    Il existe de nombreux outils pour craquer des mots de passe. N'hésitez pas à aller consulter notre fichier texte Secret accesible depuis l'URL... Par ailleurs, connaissez vous la bibliotèque `zipfile` de Python ?


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_attaque_mdp1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>

!!! danger "A retenir"

    Attention à la gestion de vos mots de passe :

     - choisissez des mots de passe robustes ;
     - ne choisissez pas des mots de passe trop proches pour la gestion d'applications sensibles ;
     - ne laissez pas traîner des informations sensibles sans les protéger avec des mots de passe efficaces.

    Pour plus d'informations, n'hésitez pas à consulter cette <a href='https://www.cybermalveillance.gouv.fr/tous-nos-contenus/bonnes-pratiques/mots-de-passe' target='_blank'>page</a>.
    
</div>

<script src='../script_chall_attaque_mdp.js'></script>
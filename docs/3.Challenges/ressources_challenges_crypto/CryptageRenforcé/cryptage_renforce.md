---
hide:
  - toc
author: à compléter
title: Cryptage renforcé
---

# Challenge : cryptage renforcé

![](../image_cryptage.png){: .center }

!!! note "Votre objectif"

    Obtenir le flag en clair caché dans un dossier hautement crypté.


??? abstract "Indice 1"
    <center><img src='../image_indice.jpg' style='width:30%;height:auto;'></center>


??? abstract "Indice 2"
    <a href='https://www.aperisolve.com/' target='_blank'>Une photo n'est pas qu'une image...</a>

??? abstract "Indice 3"

    64 32 45 : cux kw Osaq, qnl vr Tiee, jbw qw Jmsx !

    YQ94L8TG8SS8DO9R09-L9%K8769PS69C91Z6E+9V+94P9DA9 T9\$1BTF9LY6CP9\*09DN96HHVA9SLN+NTFFNZ09EOA81TJL9 U9%1TGKAAU9H09B1TZZ9ZGHLA9:X6SOBNUS-\*9UAIG09LL9GZ9-PTZN8\*S9\$L9-JTQ1SZFAWJ9NY6WVM\$M8J6HTG981SNT80T9QASNY6BN9

    efwjravfe FK5 vr elb : 621c2963ym87v182r3vm4o6336689288

    stpkvj upp+ike

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_cryptage_renforce1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>

!!! danger "A retenir"

    Il existe de nombreuses techniques pour chiffrer une information.

    Pour plus d'informations, n'hésitez pas à consulter cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/methodes_chiffrement/' target='_blank'>page</a>.

</div>

<script src='../script_chall_cryptage_renforce.js'></script>
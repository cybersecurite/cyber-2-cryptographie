---
hide:
  - toc
author: à compléter
title: Danger immédiat
---

# Challenge : danger immédiat

![](../image_chall_danger_immediat.png){: .center }

Vous êtes un expert en cybersécurité affecté à une équipe de la <a href='https://www.dgse.gouv.fr/fr' target='_blank'>DGSE</a> en charge de suivre les activités d’un certain nombre de groupes d’activistes.

**13 mai 2024**

L’un de vos équipiers vient de vous apporter un message échangé entre les membres haut placés d'un de ces groupes. Celui-ci semble indiquer la date et le lieu d’une prochaine action violente.

![](../texte_chall_danger_immediat.png){: .center }

!!! note "Votre objectif"
    Déchiffrer le message le plus rapidement possible avant qu’il ne soit trop tard.

    Format du flag : <span style='font-style:italic;'>24-avril-2020_france-paris-pont-neuf</span> 

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<p>
    <form id="form_danger_immediat1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>


<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vos supérieurs ont pu contacter dans les temps leurs holomogues suisses et tous les membres du groupe ont été arrêtés avant qu'ils ne puissent passer à l'action. Bien joué !</p>

!!! danger "A retenir"

    Sur internet, toute machine est localisée par son adresse IP. Certaines sont publiques mais d’autres, même privées, ont des failles de sécurité permettant leur utilisation.
    
    De plus, la résolution d’une enquête utilise très souvent des approches différentes et (ici, des techniques de chiffrements et des techniques d’OSINT).
    
</div>

<script src='../script_chall_danger_immediat.js'></script>
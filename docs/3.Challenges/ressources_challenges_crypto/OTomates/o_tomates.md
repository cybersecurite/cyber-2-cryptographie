---
hide:
  - toc
author: à compléter
title: O Tomates
---

# Challenge : o tomates (épicé)

![](../chall_o_tomate_intro.png){: .center }

Le précieux paquet dans sa main, Roxane sentit tout à coup une présence dans son dos. Elle se retourna vivement et aperçu au fond de la pièce un homme masqué à la silhouette familière. C'était l'ombre de son passé : un ancien collègue écarté récemment pour des malversations artistiques. Elle comprit alors qu'il était derrière tout cela, motivé par la jalousie et la rancoeur.

Alors qu'il s'apprêtait à quitter le lieu, les sirènes des policiers retentirent dans la nuit...

Avec un petit sourire, Roxane lui dit alors : "C'est fini Taz ! Tu n'aurais jamais dû revenir..."

Il se retourna lentement et lui répondit : "C'est ce que tu crois ma chère Roxane, mais la partie ne fait que commencer..."

Brusquement, il disparut dans le noir...

!!! note "Votre objectif"
    Aider Roxane à rattraper Taz avant qu'il ne s'échappe 

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>

<h2>Enigme 1</h2>
    <p>Roxane se précipita vers l'endroit où Taz venait de disparaître. Un carreau avait été descellé du sol et laissait entrevoir un escalier en colimaçon. A la lueur de la lampe de son téléphone, elle le descendit et se retrouva devant une porte fermée par un digicode :
<center><img src='../chall_o_tomate_digicode_porte.png' style='width:20%;height:auto;'></center>
<br>
Sur la porte, elle découvrit ce message : <br><br>
<center><img src='../chall_o_tomate_tableau_porte.png' style='width:60%;height:auto;'></center>
<br>
Quel code doit saisir Roxane sur le digicode pour ouvrir la porte ?</p>


<p>
    <form id="form_OTomates1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>
</div>


<div id="Partie2" hidden>
<br>
<h2>Enigme 2</h2>
<p>
Roxane poussa la porte et se retrouva dans une grotte composée de multiples salles. Devant elle, elle remarqua une plaque accrochée au mur :<br><br>
<center><img src='../chall_o_tomate_plaque.png' style='width:auto;height:auto;'></center>
<br>
Par terre, un petit morceau de papier, sans doute tombé de la poche de Taz, contenait ce message manuscrit : <br><br>
<center><img src='../chall_o_tomate_message.png' style='width:auto;height:auto;'></center>
<br>
Quel est le numéro de la salle où se situe la sortie de la grotte ?</p>
	
<p>
    <form id="form_OTomates2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte2'></p>

</div>

<div id="Partie3" hidden>
<br>
<h2>Enigme 3</h2>
<p>En arrivant vers l'entrée de la salle 7, elle entendit la voix de Taz lui crier :
<br><br>
<span style='font-style:italic;'>&laquo; Tu es très forte Roxane, mais tu ne m'attrapera jamais ! &raquo;</span>
<br><br>
Puis le bruit d'une lourde porte d'acier se refermant lui arriva aux oreilles. Elle pénétra vivement dans la salle et se précipita vers la porte sur laquelle elle trouva ce digicode très particulier :<br><br>
<center><img src='../chall_o_tomate_code_porte.png' style='width:auto;height:auto;'></center>
<br>Que doit saisir Roxane pour ouvrir cette porte ?
</p>

<p>
    <form id="form_OTomates3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte3'></p>
</div>

<div id="Partie4" hidden>
<br>
<h2>Enigme 4</h2>
<p>Une fois la porte ouverte, elle courut dans un long tunnel humide. Le bruit de la mer parvint à ses oreilles et elle déboucha dans une grotte.<br><br>
<center><img src='../chall_o_tomate_grotte.png' style='width:50%;height:auto;'></center>
<br>Comment s'appelle cette grotte ?
<br><br>
<span style='font-style:italic;'>Format du flag : grotte-de-lascaux</span>
</p>

<p>
    <form id="form_OTomates4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte4'></p>
</div>

<div id="Final" markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>
Roxane sortit de la grotte et monta sur les rochers. Au large, elle aperçut Taz sur un petit voilier. La voyant, il se leva et lui cria :
<br><br>
<span style='font-style:italic;'>&laquo; Au revoir Roxane, et non adieu ! Nous nous reverrons... &raquo;</span>
<br><br>
Et il éclata de rire...</p>

!!! danger "A retenir"

    Il existe de nombreuses techniques pour chiffrer une information. Pour plus d'informations, n'hésitez pas à consulter cette <a href='https://cybersecurite.forge.apps.education.fr/cyber/2.Boite_outils/methodes_chiffrement/' target='_blank'>page</a>.

    Les expressions régulières et les automates sont utilisés pour vérifier et automatiser des données ou des processus. Les regex valident des formats spécifiques, comme les emails, tandis que les automates structurent des actions en étapes. En cybersécurité, ils aident à contrôler la qualité des données et à détecter des comportements suspects.

</div>

<script src='../script_chall_o_tomates.js'></script>
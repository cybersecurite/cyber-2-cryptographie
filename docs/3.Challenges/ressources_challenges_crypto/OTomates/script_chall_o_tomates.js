const flags = {'OTomates1':['733828466',1,1],
	       'OTomates2':['7',2,1],
		   'OTomates3':['^[E-G]{2}[0-7][Aab]{1,2}#$',3,1],
	       'OTomates4':['grotte-des-korrigans',4,0]}; // [flag, numéro de la partie dans le challenge, partie suivante ou pas]


function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (flags[f_name][1] !== 3)
    {
        if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
        {
        texte.innerHTML = 'Bravo ! Flag validé !';
            texte.style.color = 'black';
            if(flags[f_name][2]===0) // Réussite du dernier challenge
        {
            document.getElementById('Final').hidden = false;
        }
        else
        {
            const val_part = flags[f_name][1]+flags[f_name][2];
            document.getElementById('Partie'+val_part.toString()).hidden = false;
        }
        }
        else 
        {
        texte.innerHTML = 'Flag incorrect ! Réessayez !';
            texte.style.color = 'red';
        }
    }
    else
    {
        const exp_reg = new RegExp(flags[f_name][0]);
        if (exp_reg.test(f.flag.value)) // Réussite à un challenge
        {
            texte.innerHTML = 'Bravo ! Flag validé !';
            texte.style.color = 'black';
            if(flags[f_name][2]===0) // Réussite du dernier challenge
            {
                document.getElementById('Final').hidden = false;
            }
            else
            {
                const val_part = flags[f_name][1]+flags[f_name][2];
                document.getElementById('Partie'+val_part.toString()).hidden = false;
            }
        }
        else 
        {
            texte.innerHTML = 'Flag incorrect ! Réessayez !';
            texte.style.color = 'red';
        }
    }
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_OTomates1.addEventListener( "submit", check_and_update);
form_OTomates2.addEventListener( "submit", check_and_update);
form_OTomates3.addEventListener( "submit", check_and_update);
form_OTomates4.addEventListener( "submit", check_and_update);
---
hide:
  - toc
author: à compléter
title: Zip, zip, zip hourra !
---

# Challenge : zip, zip, zip hourra ! (épicé)

![](../chall_zip.png){: .center }

Responsable d'une équipe dans une agence de cybersécurité, vous venez de recevoir de la part de votre agent ce <a href='../ultra_secret.zip' download='ultra_secret.zip'>fichier</a> contenant le mot de passe administrateur d'un serveur distant contenant des données sensibles volées que vous êtes chargé de détruire avant qu'elles ne tombent entre de mauvaises mains.

!!! note "Votre objectif"
    Récupérer le mot de passe administrateur 

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>

<h2>Enigme 1</h2>
    <p>Extraire le document du fichier zip que vous venez de télécharger.<br><br>
    Le flag est le mot de passe du fichier zip.<br><br>
    <span style='font-style:italic;'>Indice : connaissez-vous <a href='../rockyou.txt' download='rockyou.txt'>rockyou</a> ?</span></p>


<p>
    <form id="form_zip1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>
</div>


<div id="Partie2" hidden>
<br>
<h2>Enigme 2</h2>
<p>
Maintenant que vous venez de récupérer le document contenu dans le fichier zip, vous devez récupérer le mot de passe administrateur du serveur.<br><br>
Le flag est ce mot de passe.</p>
	
<p>
    <form id="form_zip2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte2'></p>

</div>


<div id="Final" markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>
Vous avez réussi à accéder au serveur et à détruire les données sensibles qui avaient été volées.</p>

!!! danger "A retenir"

    Attention à la gestion de vos mots de passe :

    - choisissez des mots de passe robustes ;
    - ne choisissez pas des mots de passe trop proches pour la gestion d'applications sensibles ;
    - ne laissez pas traîner des informations sensibles sans les protéger avec des mots de passe efficaces.
    
    Pour plus d'informations, n'hésitez pas à consulter cette page : [Protégez vos accès avec des mots de passe solides](https://www.cybermalveillance.gouv.fr/tous-nos-contenus/bonnes-pratiques/mots-de-passe)

</div>

<script src='../script_chall_zip_spicy.js'></script>